package org.zabbix.template.generator.objects

rule "Rule 1: automatically add type=SNMP if type is not populated"
	agenda-group "populate"
	dialect "mvel"
when
	$m : Metric (type == null, snmpObject !=null, oid != null )
then
    logger.debug(marker,"RULE1:Adding type=SNMP for "+$m.name);
    modify($m) { type = Type.SNMP };
end

rule "Rule 1.2: automatically add type=SNMP if type is not populated"
	agenda-group "populate"
	dialect "mvel"
when
	$dr : DiscoveryRule (type == null, oid != null )
then
    logger.debug(marker,"RULE1.2: Adding type=SNMP for "+$dr.name);
	modify($dr) { type = Type.SNMP }; 
end


/*
TODO remove this rule.
regenerate defined key if it is item prototype.
This rule must go before anything else
*/
rule "Rule 2.1: Generate metric key for SNMP type"
	agenda-group "populate"
	salience 999
	no-loop
	dialect "mvel"
when
	$m : Metric ( snmpObject != null, key != null, key not contains '[', key not contains ']', discoveryRule != null)
then
    logger.info(marker,"RULE2.1: There is a 'key' statically defined but item is prototype(LLD): for "+$m.name+" "+$m.key+" so let's regenerate it...");
    modify( $m ) {
            key = $m.id+"["+$m.snmpObject+"]"
    };
end

//This rule must go before anything else
rule "Rule 2: Generate metric key for SNMP type"
	agenda-group "populate"
	salience 999
	no-loop
	dialect "mvel"
when
	$m : Metric ( key == null, snmpObject != null, id != null)
then
    logger.debug(marker,"RULE2: There is no 'key' for "+$m.name+" so let's generate it...");
    modify( $m ) {
            key = $m.id+"["+$m.snmpObject+"]"
    };
end


rule "Rule 3: prefix Name with resource"
	agenda-group "populate"
	dialect "mvel"
	no-loop
when
	$m : Metric ( resource != null, name not contains '__RESOURCE__')
then
    logger.debug(marker,"RULE3: Changing name to 'resource: $m.name' for "+$m.name);
    modify( $m ) { 
    		name = $m.resource + ": " + $m.name
    	};
end

rule "Rule 3a: generate each trigger name(discovery rules, prefix with resource)"
	agenda-group "populate"
	dialect "mvel"
	no-loop
when
    $m : Metric ($key: key != null, resource != null, $triggers: triggers)
    $tr: Trigger ($e: expression!=null, name not contains '__RESOURCE__') from $triggers 
then
    logger.debug(marker,"Generating trigger name for "+ $tr.name);
    $tr.name = $m.resource + ": " + $tr.name;
end

rule "Rule 3b: generate each graph name(discovery rules, prefix with resource)"
	agenda-group "populate"
	dialect "mvel"
	no-loop
when
    $m : Metric ($key: key != null, resource != null, $graphs: graphs)
    $gr: Graph (resource == null, name not contains '__RESOURCE__') from $graphs
then
    logger.info(marker,"Generating graph name for "+ $gr.name);
    $gr.name = $m.resource + ": " + $gr.name;
end

rule "Rule 3c: override each graph name(discovery rules, prefix with resource)"
	agenda-group "populate"
	dialect "mvel"
	no-loop
when
    $m : Metric ($key: key != null, resource != null, $graphs: graphs)
    $gr: Graph (resource != null, name not contains '__RESOURCE__') from $graphs
then
    logger.info(marker,"Generating overrided graph name for "+ $gr.name);
    $gr.name = $gr.resource + ": " + $gr.name;
end


rule "Rule 8: purge original description(description)"
	agenda-group "populate"
	salience 101
	dialect "mvel"
when
	$m : Metric (vendorDescription != null )
then
    logger.debug(marker,"Generating description(description) for "+$m);
    modify( $m ) {
    		description = null;
    	};
end

rule "Rule 9: generate each item description(mib)"
	agenda-group "populate"
	salience 100
when
	$m : Metric (mib != null, type == Type.SNMP)
then
    logger.debug(marker,"Generating description(mib) for "+$m);
	if ($m.getDescription() != null) {
		modify( $m ) {
    		setDescription( "MIB: " + $m.getMib()+"\n"+$m.getDescription() )
	    };
	}
	else {
		modify( $m ) {
			setDescription( "MIB: " + $m.getMib()+"\n" )
		};
	}
end

rule "Rule 10: generate each item description"
	agenda-group "populate"
	salience 99
when
	$m : Metric (vendorDescription != null)
then
    logger.debug(marker,"Generating description(vendorDescription) for "+$m);
	if ($m.getDescription() != null) {
		modify( $m ) {
    		setDescription( $m.getDescription() + $m.getVendorDescription() )
	    };
	}
	else {
		modify( $m ) {
			setDescription( $m.getVendorDescription() )
		};
	}
end

rule "Rule 11: generate each item description"
	agenda-group "populate"
	salience 98
when
	$m : Metric (ref != null)
then
    logger.debug(marker,"Generating description(ref) for "+$m);
    modify( $m ) { 
    		setDescription( $m.getDescription() + "\nReference: " + $m.getRef() );
    	};
end

/*
 It's not safe to map item prototypes to inventory. So, that kind of linked should be removed.
*/
rule "Rule: purge inventory_link for item prototypes"
	agenda-group "populate"
	salience 101
when
	$dr: DiscoveryRule(singleton !=true)
	$m : Metric (discoveryRule != null, inventoryLink != InventoryLink.NONE) from $dr.metrics
then
    logger.warn(marker,"Purging inventory link for item prototype:"+$m.getId());
    $m.setInventoryLink(InventoryLink.NONE);
end

rule "Rule: reset delay to 0 for DEPENDENT or zabbix trapper ITEMS"
	agenda-group "populate"
	salience 101
	dialect "mvel"
when
	$m : Metric (type == Type.DEPENDENT || type == Type.TRAP)
then
    logger.debug(marker,"Resetting delay to 0 for item {} of type {}", $m.id, $m.type);
    $m.delay = 0;
end

rule "Rule: reset delay to 0 for DEPENDENT or zabbix trapper discovery rule"
	agenda-group "populate"
	salience 101
	dialect "mvel"
when
	$dr : DiscoveryRule (type == Type.DEPENDENT || type == Type.TRAP)
then
    logger.debug(marker,"Resetting delay to 0 for for discovery {} of type {}", $dr.name, $dr.type);
    $dr.delay = 0;
end

rule "Rule: reset trends to 0 non-numeric values"
	agenda-group "populate"
	salience -100
	dialect "mvel"
when
	$m : Metric (
		(valueType != ValueType.UNSIGNED && valueType != ValueType.FLOAT)
	)
then
    logger.debug(marker,"Resetting trends to 0 for item {}", $m.id);
    $m.trends = "0";
end

rule "Rule: reset oid field for non SNMP items"
	agenda-group "populate"
	salience -1
	dialect "mvel"
when
	$m : Metric (type != Type.SNMP, oid != null)
then
    logger.debug(marker,"Resetting oid for {}", $m.id);
	modify( $m ) {
		oid = null;
	};
end

// commented out to avoid broken graphs
// rule "Rule: set update interval to 30s for health-checks(metrics with value_maps) and add discard with 5m"
// 	agenda-group "populate"
// 	salience -100
// 	dialect "mvel"
// when
// 	$m : Metric (
// 		valueMap != null,
// 		preprocessing.size() == 0
// 	)
// 	// TODO add comparator to check whether discard already present then remove size() restriction
// then
//     logger.debug(marker,"Setting update interval to 30s for item {}", $m.id);
//     $m.delay= "30s";
// 	$m.history= "14d";
// 	discard = new PreprocessingStep();
// 	discard.type = PreprocessingStepType.DISCARD_UNCHANGED_HEARTBEAT;
// 	discard.params = "5m";
// 	$m.preprocessing.add(discard);
// end


rule "Rule: add discard with 1h/1d heathbeat for inventory checks. Enforce 14d for history"
	agenda-group "populate"
	salience -100
	dialect "mvel"
when
	$m : Metric (
		(inventoryLink != InventoryLink.NONE ||
		group == Metric$Group.Inventory)
	)
	// TODO add comparator to check whether discard already present then remove size() restriction
then
    logger.debug(marker,"Setting update interval to 1h for item {}", $m.id);
	
	$m.delay= "1h";
	$m.history= "2w";
	discard = new PreprocessingStep();
	discard.type = PreprocessingStepType.DISCARD_UNCHANGED_HEARTBEAT;
	discard.params = "1d";
	if (!($m.preprocessing contains discard)) {
		logger.debug(marker,"Preprocessing does not contain {} {}, {}",$m.id, discard.type, $m.preprocessing);
		$m.preprocessing.add(discard);
	} else {
		logger.debug(marker,"Preprocessing contains {} {}, {}",$m.id, discard.type, $m.preprocessing);
	}
end


// Don't forget to generate {#SINGLETON macro in LLD preprocessing (such as JS preproccessing)}
rule "Rule: generate each graph name for singletons(discovery rules, add {#SINGLETON} suffix)"
	agenda-group "populate"
	dialect "mvel"
	no-loop
when
	$dr : DiscoveryRule ($metrics: metrics, singleton == true)
    $m : Metric (discoveryRule !=null, $graphs: graphs) from $metrics
    $gr: Graph () from $graphs
then
    logger.info(marker,"Adding {#SINGLETON} macro to graph name for "+ $gr.name);
	$gr.name = $gr.name+"{#SINGLETON}";
end