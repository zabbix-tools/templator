package org.zabbix.template.generator.objects;

public class MetricPrototypeNotFoundException extends RuntimeException {

	public MetricPrototypeNotFoundException(String m) {
		super(m);
	}

}
