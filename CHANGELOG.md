# Templator changelog

## Unreleased

### Bug Fixes

* 

### Added

* Added ability to override `_resource` concatenation in item, trigger and graph names. use `__RESOURCE__` in prototype name instead for more beatiful names.

## v0.33

This was the latest release without changelog.
